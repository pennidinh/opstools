#!/usr/bin/ruby

require 'json'

tableNames = JSON.parse(`aws dynamodb list-tables --region us-west-2`)['TableNames']
subdomainAssignmentsTableName = tableNames.find{ |tableName| tableName.include? 'SubdomainAssignmentsDynamoDBTable' }
puts "subdomainAssignmentsTableName: #{subdomainAssignmentsTableName}"

puts "What pennidinh subdomain (for johndoe.pennidinh.com, enter johndoe without quotes)?"
subdomain = gets.chomp

puts "evaluating for subdomain #{subdomain}"
["internal-#{subdomain}.pennidinh.com", "#{subdomain}.pennidinh.com"].each do |subdomainName|
  subdomainExistingOwnershipRaw = `aws dynamodb get-item --region us-west-2 --table-name "#{subdomainAssignmentsTableName}" --key "{\\"Subdomain\\": {\\"S\\": \\"#{subdomainName}\\"}}"`
  subdomainExistingOwnership = JSON.parse(subdomainExistingOwnershipRaw) rescue nil
  
  existingOwner = subdomainExistingOwnership && subdomainExistingOwnership['Item'] && subdomainExistingOwnership['Item']['ClientId'] && subdomainExistingOwnership['Item']['ClientId']['S'] ? subdomainExistingOwnership['Item']['ClientId']['S'] : nil
  if existingOwner.nil?
    puts "No existing owner of #{subdomainName}"
  else
    response = ''  
    while response != 'y' && response != 'n'
      puts "Existing owner of #{existingOwner} for #{subdomainName}. Continue? (y/n)"
      response = gets.chomp
    end
  
    if response == 'n'
      exit
    end
  end
end

puts "What client ID should own #{subdomain}?"
clientID = gets.chomp

response = ''
while response != 'y' && response != 'n'
  puts "Would you like to assign #{clientID} to #{subdomain}? (y/n)"
  response = gets.chomp
end

if response == 'n'
  exit
end

existingSubdomainsForClientIdRaw = `aws dynamodb query --region us-west-2 --table-name "#{subdomainAssignmentsTableName}" --index-name "ClientId" --key-conditions "{\\"ClientId\\": {\\"AttributeValueList\\": [{\\"S\\": \\"#{clientID}\\"}], \\"ComparisonOperator\\": \\"EQ\\"}}"`
existingSubdomainsForClientId = JSON.parse(existingSubdomainsForClientIdRaw)
if existingSubdomainsForClientId['Count'] != 0
  puts "Existing subdomains are owned by ClientID #{clientID}. Please first remove these assignments."
  puts existingSubdomainsForClientIdRaw
  exit
end

response = ''
while response != 'y' && response != 'n'
  puts 'Is this a shared proxy? (y/n)'
  response = gets.chomp
end
isSharedProxy = response == 'y'

["#{subdomain}.pennidinh.com", "internal-#{subdomain}.pennidinh.com"].each do |subdomainName|
  puts `aws dynamodb put-item --region us-west-2 --table-name "#{subdomainAssignmentsTableName}" --item "{\\"IsProxy\\":{\\"BOOL\\": #{isSharedProxy ? 'true' : 'false'}},\\"ClientId\\":{\\"S\\": \\"#{clientID}\\"},\\"Subdomain\\": {\\"S\\": \\"#{subdomainName}\\"}}"`
end

puts 'Completed successfully (if no errors printed)'
