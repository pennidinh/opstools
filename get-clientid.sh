#!/usr/bin/ruby

require 'json'

tableNames = JSON.parse(`aws dynamodb list-tables --region us-west-2`)['TableNames']
subdomainAssignmentsTableName = tableNames.find{ |tableName| tableName.include? 'SubdomainAssignmentsDynamoDBTable' }
puts "subdomainAssignmentsTableName: #{subdomainAssignmentsTableName}"

puts "What pennidinh subdomain (for johndoe.pennidinh.com, enter johndoe without quotes)?"
subdomain = gets.chomp

puts "evaluating for subdomain #{subdomain}"
["internal-#{subdomain}.pennidinh.com", "#{subdomain}.pennidinh.com"].each do |subdomainName|
  subdomainExistingOwnershipRaw = `aws dynamodb get-item --region us-west-2 --table-name "#{subdomainAssignmentsTableName}" --key "{\\"Subdomain\\": {\\"S\\": \\"#{subdomainName}\\"}}"`
  subdomainExistingOwnership = JSON.parse(subdomainExistingOwnershipRaw) rescue nil
  
  existingOwner = subdomainExistingOwnership && subdomainExistingOwnership['Item'] && subdomainExistingOwnership['Item']['ClientId'] && subdomainExistingOwnership['Item']['ClientId']['S'] ? subdomainExistingOwnership['Item']['ClientId']['S'] : nil
  if existingOwner.nil?
    puts "No existing owner of #{subdomainName}"
  else
    puts "Existing owner of #{existingOwner} of #{subdomainName}"
  end
end
