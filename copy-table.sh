#!/usr/bin/ruby

require 'json'

tableNames = JSON.parse(`aws dynamodb list-tables --region us-west-2`)['TableNames']
subdomainAssignmentsTableName = tableNames.find{ |tableName| tableName.include? 'SubdomainAssignmentsDynamoDBTable' }
puts "subdomainAssignmentsTableName: #{subdomainAssignmentsTableName}"

command = 'aws dynamodb scan --table-name PenniDinh_Subdomain --region us-west-2'
puts command
result = `#{command}`
puts result
resultJson = JSON.parse(result)
items = resultJson['Items']
items.each do |item|
  File.open('item.json', 'w') { |file| file.write(JSON.generate(item)) }
  command = 'aws dynamodb put-item --table-name #{subdomainAssignmentsTableName} --region us-west-2 --item file://item.json'
  puts command
  result = `#{command}`
  puts result
end
File.delete('item.json')
