#!/usr/bin/ruby

require 'json'

tableNames = JSON.parse(`aws dynamodb list-tables --region us-west-2`)['TableNames']
sharedProxyAssignmentsTableName = tableNames.find{ |tableName| tableName.include? 'SharedProxyAssignmentsDynamoDBTable' }
puts "sharedProxyAssignmentsTableName: #{sharedProxyAssignmentsTableName}"

puts "What subdomain (ex. subdomain for johndoe.pennidinh.com would be johndoe without quotes)?"
subdomain = gets.chomp

response = ''
while response != 'ssh' && response != 'https'
  puts 'What proxy type (ssh or https)?'
  response = gets.chomp
end
service = response

proxySocketExistingKeyRaw = `aws dynamodb get-item --region us-west-2 --table-name "#{sharedProxyAssignmentsTableName}" --key "{\\"externalSubdomain\\": {\\"S\\": \\"#{subdomain}.pennidinh.com\\"},\\"service\\": {\\"S\\": \\"#{service}\\"}}"`
puts proxySocketExistingKeyRaw
proxySocketExistingKey = JSON.parse(proxySocketExistingKeyRaw) rescue nil

existingProxySocket = proxySocketExistingKey && proxySocketExistingKey['Item'] ? 1 : nil
if existingProxySocket.nil?
  puts "No existing proxy socket for #{subdomain}"
else
  response = ''  
  while response != 'y' && response != 'n'
    puts "Existing proxy socket #{proxySocketExistingKeyRaw}. Continue? (y/n)"
    response = gets.chomp
  end

  if response == 'n'
    exit
  end
end

response = ''
while (!Integer(response).is_a?(Integer) rescue true)
  puts "What port number?"
  response = gets.chomp
end
port = Integer(response)

puts 'What shared proxy hostname? (ex. shared for shared.pennidinh.com)'
hostname = gets.chomp + '.pennidinh.com'

response = ''
while response != 'y' && response != 'n'
  puts "Would you like to assign #{subdomain} to #{hostname} on port #{port} of service type #{service}?"
  response = gets.chomp
end

if response == 'n'
  exit
end

# TODO assert subdomain is valid
# TODO assert shared proxy hostname is valid

puts `aws dynamodb put-item --region us-west-2 --table-name "#{sharedProxyAssignmentsTableName}" --item "{\\"externalSubdomain\\":{\\"S\\": \\"#{subdomain}.pennidinh.com\\"},\\"service\\":{\\"S\\": \\"#{service}\\"},\\"hostname\\": {\\"S\\": \\"#{hostname}\\"},\\"port\\": {\\"N\\": \\"#{port}\\"}}"`

puts 'Successfully completed (if no error message)'
