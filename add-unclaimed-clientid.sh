#!/usr/bin/ruby

require 'json'

tableNames = JSON.parse(`aws dynamodb list-tables --region us-west-2`)['TableNames']
unclaimedClientIdsTableName = tableNames.find{ |tableName| tableName.include? 'UnclaimedClientIdsDynamoDBTable' }

puts "Using unclaimed client ID table name #{unclaimedClientIdsTableName}"

puts "What client ID would you like to add?"
clientId = gets.chomp

puts `aws dynamodb put-item --region us-west-2 --table-name "#{unclaimedClientIdsTableName}" --item "{\\"ClientId\\": {\\"S\\": \\"#{clientId}\\"}}"`
