#!/usr/bin/ruby

REGIONS = ['us-east-1', 'us-west-2', 'us-west-1']

require 'json'
require 'socket'

tableNames = JSON.parse(`aws dynamodb list-tables --region us-west-2`)['TableNames']
sharedProxyAssignmentsTableName = tableNames.find{ |tableName| tableName.include? 'SharedProxyAssignmentsDynamoDBTable' }
puts "sharedProxyAssignmentsTableName: #{sharedProxyAssignmentsTableName}"

puts "which subdomain?"
externalSubdomain = gets.chomp
service = 'ssh'

command = 'aws dynamodb get-item --region us-west-2 --table-name "#{sharedProxyAssignmentsTableName}" --key "{\"externalSubdomain\": {\"S\": \"' + externalSubdomain + '.pennidinh.com\"}, \"service\":{\"S\":\"' + service + '\"}}"'
puts command
hostnameAndPortRaw = `#{command}`
puts hostnameAndPortRaw
hostnameAndPort = JSON.parse(hostnameAndPortRaw)

proxyDomainName = hostnameAndPort['Item']['hostname']['S']
port = hostnameAndPort['Item']['port']['N'].to_s

publicIP = IPSocket.getaddress(proxyDomainName)
puts "Public IP used #{publicIP}"

reservation = nil
region = nil
REGIONS.each do |regionIter|
  command = "aws ec2 --region #{regionIter} describe-instances --filters Name=ip-address,Values=#{publicIP}"
  puts command
  describeInstancesStr = `#{command}`
  describeInstances = JSON.parse(describeInstancesStr)
  reservations = describeInstances['Reservations']
  if reservations.length == 1
    reservation = reservations[0]
    region = regionIter
  end
end
if reservation.nil?
  raise "Could not find reservation for that public IP in any region: #{REGIONS}"
end

instances = reservation['Instances']
if instances.length != 1
    raise "Expected 1 instance result back. Instead got: " + describeInstancesStr
end
instanceId = instances[0]['InstanceId']
puts "InstanceID #{instanceId}"
if instanceId.nil? || instanceId == ''
    raise 'Expected to have a non-blank instance ID'
end
az = instances[0]['Placement']['AvailabilityZone']
puts "Availability Zone #{az}" 
if az.nil? || az == ''
    raise 'Expected to have a non-blank Availability Zone' 
end

command = "aws ec2-instance-connect --region #{region} send-ssh-public-key     --instance-id  #{instanceId}    --availability-zone #{az}     --instance-os-user ec2-user     --ssh-public-key file://~/.ssh/id_rsa.pub"
puts command
`#{command}`

command = "ssh -t ec2-user@#{publicIP} 'cd /pennidinhsharedproxy ; sudo /usr/local/bin/docker-compose exec sshproxyserver bash -c \"ssh root@localhost -p #{port}\"'"
puts command
exec command
