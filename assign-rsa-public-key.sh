#!/usr/bin/ruby

require 'json'

tableNames = JSON.parse(`aws dynamodb list-tables --region us-west-2`)['TableNames']
clientPublicKeysTableName = tableNames.find{ |tableName| tableName.include? 'ClientPublicKeysDynamoDBTable' }
puts "clientPublicKeysTableName: #{clientPublicKeysTableName}"

puts "What client ID?"
clientId = gets.chomp

puts "evaluating for clientId #{clientId}"
clientIdExistingPubKeyRaw = `aws dynamodb get-item --region us-west-2 --table-name "#{clientPublicKeysTableName}" --key "{\\"ClientId\\": {\\"S\\": \\"#{clientId}\\"}}"`
clientIdExistingPubKey = JSON.parse(clientIdExistingPubKeyRaw) rescue nil

existingPubKey = clientIdExistingPubKey && clientIdExistingPubKey['Item'] && clientIdExistingPubKey['Item']['ClientId'] && clientIdExistingPubKey['Item']['ClientId']['S'] ? clientIdExistingPubKey['Item']['ClientId']['S'] : nil
if existingPubKey.nil?
  puts "No existing pub key for #{clientId}"
else
  response = ''  
  while response != 'y' && response != 'n'
    puts "Existing pub key #{existingPubKey} for #{clientId}. Continue? (y/n)"
    response = gets.chomp
  end

  if response == 'n'
    exit
  end
end

sshPubKey = ''
nextLn = ''
while nextLn != 'end'
  sshPubKey = sshPubKey + nextLn 
  puts "What is the new SSH public key? (type in \"end\" on an empty line to complete)"
  nextLn = gets.chomp
end

if !sshPubKey.start_with? "-----BEGIN PUBLIC KEY-----"
  puts "Public keys must start with \"-----BEGIN PUBLIC KEY-----\""
  exit
end
if !sshPubKey.end_with? "-----END PUBLIC KEY-----"
  puts "Public keys must start with \"-----END PUBLIC KEY-----\""
  exit
end

response = ''
while response != 'y' && response != 'n'
  puts "Would you like to assign #{sshPubKey} to #{clientId}? (y/n)"
  response = gets.chomp
end

if response == 'n'
  exit
end

puts `aws dynamodb put-item --region us-west-2 --table-name "#{clientPublicKeysTableName}" --item "{\\"ClientId\\":{\\"S\\": \\"#{clientId}\\"},\\"PublicKey\\": {\\"S\\": \\"#{sshPubKey}\\"}}"`

puts 'Completed successfully (if no errors printed)'
