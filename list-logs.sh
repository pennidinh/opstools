#!/usr/bin/env ruby

require 'json'

resultsRaw = `aws s3api list-objects --bucket pennidinh-diagnostics-reports`
#puts resultsRaw
results = JSON.parse(resultsRaw)
s3Keys = results['Contents'].map {|obj| obj['Key'] }.sort.reverse
puts s3Keys

